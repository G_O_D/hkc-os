;-------------------------------------------------------------------------------------------------
; AUTHOR : Muhammad Quwais Safutra, Gilang Ramadan
; TODO   : Menampilkan Semua Output Dari Setiap Booter
;-------------------------------------------------------------------------------------------------



    SECTION .text

    global asm_tick        
    section .text
asm_tick:

    test rdi, rdi           
    jnz quit


    
    xor rsi, rdi
    xor rdi, rsi
    xor rsi, rdi

    add rdi, 160*4

    mov rsi, red
    call set_pixel
    
    add rdi, 1
    mov rsi, green
    call set_pixel

    add rdi, 1
    mov rsi, blue
    call set_pixel
 
    mov     rax, 0          
    ret                     
        
quit:
    mov     rax, 1          
    ret                     

set_pixel:
    mov ecx, 3
    rep movsb
    ret
    
    SECTION .data
red:    db 0xff, 0x00, 0x00
green:  db 0x00, 0xff, 0x00
blue:   db 0x00, 0x00, 0xff

width:  db 160
height: db 90

        global asm_tick         
        section .text
asm_tick:

        test    rdi, rdi        
        jnz quit
        
        mov     rax, 0          
        ret                     
        
quit:
        mov     rax, 1          
        ret                     
[bits 16]
switch_to_pm:
    cli 
    lgdt [gdt_descriptor] 
    mov eax, cr0
    or eax, 0x1 
    mov cr0, eax
    jmp CODE_SEG:init_pm 

[bits 32]
init_pm: 
    mov ax, DATA_SEG 
    mov ds, ax
    mov ss, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    mov ebp, 0x90000 
    mov esp, ebp

    call BEGIN_PM 
