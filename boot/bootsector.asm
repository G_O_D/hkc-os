;-------------------------------------------------------------------------------------------------
; AUTHOR : Muhammad Quwais Safutra
; TODO   : Kernel
;-------------------------------------------------------------------------------------------------



    SECTION .text

    global asm_tick        
    section .text
asm_tick:

    test rdi, rdi           
    jnz quit


    
    xor rsi, rdi
    xor rdi, rsi
    xor rsi, rdi

    add rdi, 160*4

    mov rsi, red
    call set_pixel
    
    add rdi, 1
    mov rsi, green
    call set_pixel

    add rdi, 1
    mov rsi, blue
    call set_pixel
 
    mov     rax, 0          
    ret                     
        
quit:
    mov     rax, 1          
    ret                     

set_pixel:
    mov ecx, 3
    rep movsb
    ret
    
    SECTION .data
red:    db 0xff, 0x00, 0x00
green:  db 0x00, 0xff, 0x00
blue:   db 0x00, 0x00, 0xff

width:  db 160
height: db 90